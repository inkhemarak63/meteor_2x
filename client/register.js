// Main Layout
import './layouts/mainLayout';

// *** components *** //
import './components/loading/loading';
import './components/header/header';
import './components/footer/footer';
import './components/404/notfound';
import './components/article/article';
import './components/table/table';

// *** Auth *** //
import './auth/login/login';
import './auth/register/register';
import './auth/profile/profile';

// *** web *** //
import './web/home/home';

// Admin
import './admin/sample/record';
