import './register.html';
Template.register.events({
    'submit form': function(event) {
        event.preventDefault();
        var isValid = ValidateForm.validate('#new-user-form');
        if (!isValid) return;
        const {
            role,
            username,
            password,
            confirm_password
        } = event.target
        const fields = {
            role     :role.value,
            username :username.value,
            password :password.value,
            cassword :confirm_password.value,
        }
        //method call save 
        Meteor.call('authResiter', fields, function (err, rs) {
            if(rs){
                Meteor.connection.setUserId(rs.user_id);
            }
        })
    }
})    
Template.register.onRendered(function() {
    console.log('rendered')
})
Template.register.onCreated(async function() {
   console.log('created')
 })
 