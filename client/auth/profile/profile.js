import './profile.html';
import { Partners } from '/libs/collections';
import Swal from 'sweetalert2';

import 'croppie/croppie.js';
import 'croppie/croppie.css';

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1500,
})

Template.profile.helpers({
    Collection: () => {
        return Partners;
    },
    resultOption: function() {
        var user = Meteor.users.findOne({ _id: Meteor.userId() });
        console.log("user");
        if(user !== undefined) return user
    },
    manageType: () => {
        Meteor.subscribe('PublishUserLogin', Meteor.userId());
        let part = Partners.findOne({ account: Meteor.userId() });
        if (part !== undefined) {
            return 'update';
        } else {
            return 'insert';
        }
    },
    ProfileDoc: () => {
        Meteor.subscribe('PublishUserLogin', Meteor.userId());
        let part = Partners.findOne({ account: Meteor.userId() });
        if (part !== undefined) return part;
    },
    showhide() {
        var status = Template.instance().messageVisible.get();
        if (status == false) {
            return "drop-collape";
        } else {
            return "drop-change";
        }
    },
});

Template.profile.events({
    'mouseenter .pro-img-circle' (event, instance) {
        instance.messageVisible.set(true);
    },
    'mouseleave .pro-img-circle' (event, instance) {
        instance.messageVisible.set(false);
    },
    'click #choose-img': (event) => {
        Router.go('/profile/photo');
    },
    'change .file-upload': function(event) {
        readURLImages($(event.target));
    },
});

Template.profile.onCreated(function onCreated() {
    this.messageVisible = new ReactiveVar(false);
});


AutoForm.addHooks('profileForm', {
    onSuccess(type, rs) {
        Toast.fire({
            icon: 'success',
            title: 'Successfully.'
        })
    },
    onError(type, err) {
        console.log(err);
        Toast.fire({
            icon: 'error',
            title: 'Something wwnt wrong.'
        })
    }
});

function readURLImages(input) {
    if (input[0].files && input[0].files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.btn-cancel').show();
            $('.btn-use-image').show();
            $('#img-info-crop').show();

            var w_viewport = 200;
            h_viewport = 200;
            w_boundary = 230;
            h_boundary = 230;

            showCropImage(w_viewport, h_viewport, w_boundary, h_boundary, e.target.result);
        }
        reader.readAsDataURL(input[0].files[0]);
    }
}

function showCropImage(w_viewport, h_viewport, w_boundary, h_boundary, base64) {
    $('#edit-pic').croppie('destroy');
    var profilePic = $('#edit-pic').croppie({
        viewport: {
            width: w_viewport,
            height: h_viewport
        },
        boundary: {
            width: w_boundary,
            height: h_boundary
        }
    });
    profilePic.croppie('bind', {
        url: base64,
    });
}