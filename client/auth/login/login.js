import './login.html';

Template.login.events({
  'submit form': function (event) {
    event.preventDefault();
    var isValid = ValidateForm.validate('#login-user-form');
    if (!isValid) return;
    var username = event.target.username.value;
    var password = event.target.password.value;
    //go login
    Meteor.loginWithPassword(username, password, function (error) {
      if (!error) {
        let authenticated = true;
       Router.go('/');
      } else {
       alert("Error")
      }
    });
  }
})    