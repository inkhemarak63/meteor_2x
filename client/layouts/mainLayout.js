import './mainLayout.html';
Template.MainLayout.helpers({
    routPrivate: function(){
        let name = Router.current().route.getName();
        let routes = ['login'];
        if (routes.indexOf(name) === -1) 
        {
            return true
        }
        return false
    }
})
Template.MainLayout.onRendered(function(event) {
    Meteor.setTimeout(function(){
        $('body').removeClass("pg-loaded")
    }, 6000);
})
