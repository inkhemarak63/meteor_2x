import { Template } from 'meteor/templating';
import { AutoForm } from 'meteor/aldeed:autoform';
import Swal from 'sweetalert2';
import { Province } from '/libs/collections';
import './record.html';

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1500,
})

Template.province_record.helpers({
    MemberList: function(event) {
        return TabularTables.MemberListTable;
    },
    Collection: () => {
        return Province;
    },
    manageType: () => {
        var id = Session.get('edit_province');
        if (id !== undefined) {
            return 'update';
        } else {
            return 'insert';
        }
    },
    provinceDoc: () => {
        var id = Session.get('edit_province');
        Meteor.subscribe('getProvinceById', id);
        var rs = Province.findOne({ _id: id });
        if (rs !== undefined) return rs;
    }
});
Template.province_record.events({
    'click #delete_province': function(event){
        var value = event.currentTarget.dataset;
		// delete on server
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 1500,
		})
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-success btn-sm btn-margin-left',
				cancelButton: 'btn btn-danger btn-sm btn-margin-left'
			},
			buttonsStyling: false
		})
		swalWithBootstrapButtons.fire({
			title: 'Confirmation!',
			text: "Are you sure to delete?",
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			reverseButtons: true
		}).then((result) => {
			if (result.isConfirmed) {
				Meteor.call('DeleteProvince', value.id, (err, rs) => {
					if (rs) {
						Toast.fire({
							icon: 'success',
							title: 'Delete Successfully.'
						})
					} else {
						Toast.fire({
							icon: 'error',
							title: 'Delete not Successfully.'
						})
					}
				});
			}
		})
    },
    'click #edit_province': function(event) {
        var dt = event.currentTarget.dataset;
        if(dt.value !== undefined);
        {
            Session.set('edit_province', dt.value);
        }
    },
});

AutoForm.hooks({
    Province: {
        before: {
            insert: function(doc) {
                setTimeout(() => {
                    $('#btn-save').attr('disabled', false)
                }, 0);

                if (!AutoForm.validateForm(this.formId)) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Something went wrong.'
                    })
                    return this.result(doc); // continue with normal submission which will fail due to validation
                }
                this.resetForm(this.formId);
                Toast.fire({
                    icon: 'success',
                    title: 'Successfully.'
                });
                return this.result(doc);
            },
        },
    }
});

AutoForm.addHooks(['Province'], {
    onSuccess(type, rs) {
        Toast.fire({
            icon: 'success',
            title: 'Successfully.'
        });
        $('#btn-save').prop('disabled', false);
        Session.set('edit_province', undefined);
    },
    onError(type, err) {
        Toast.fire({
            icon: 'error',
            title: 'Something wwnt wrong.'
        })
    },
});
