import '/configs/routes';
import '/configs/user';
import '/libs/helpers';
import '/libs/tabulars';

import { Template } from 'meteor/templating';
import { $ } from 'meteor/jquery';
import dataTablesBootstrap from 'datatables.net-bs';
dataTablesBootstrap(window, $);

import './main.html';
//publish all
Meteor.subscribe('PublishUserLogin', Meteor.userId());

if (Meteor.isClient) {
    Meteor.startup(function() {
        
    });
}
Template.body.events({
    'click #sidebar_toggle': function(){
        var sidebar = $('#sidebar');
        var padder = $('.content-padder');
        if(sidebar.css('display') == 'none' ) {
            sidebarToggle(true)
        } else {
            sidebarToggle(false)
        }
    },
    'click .content-padder': function(){
        if( $( window ).width() < 960 ) {
            sidebarToggle(false);
        }
    }
 })
Template.body.onRendered(function(event) {
    if($( window ).width() < 960) {
        sidebarToggle(false);
    }
	$( window ).resize(function() {
		resize()
	});
})
// Sidebar Toggler
function sidebarToggle(toogle) {
    var sidebar = $('#sidebar');
    var padder = $('.content-padder');
    if(toogle) {
        console.log("true");
        $('.notyf').removeAttr( 'style' );
        sidebar.css({'display': 'block', 'x': -300});
        sidebar.css({'display':'block', 'opacity': '1', 'transform':'translate(0px,0px)'})
        // sidebar.animate({
        //     opacity: 1,
        //     }, function() {
        //         sidebar.css({'display':'block','transform':'translate(0px,0px)'
        //     });
        // });
        if( $( window ).width() > 960 ) {
            padder.css({'margin-left': sidebar.css('width')});
        }
        // $('.notyf').removeAttr( 'style' );
        // sidebar.css({'display': 'block', 'x': -300});
        // sidebar.transition({opacity: 1, x: 0}, 250, 'in-out', function(){
        //     sidebar.css('display', 'block');
        // });
        // if( $( window ).width() > 960 ) {
        //     padder.transition({marginLeft: sidebar.css('width')}, 250, 'in-out');
        // }

    } else {
        console.log("false");
        $('.notyf').css({width: '90%', margin: '0 auto', display:'block', right: 0, left: 0});
        sidebar.css({'display': 'block', 'x': '0px'});
        sidebar.css({'display':'none', 'opacity': '0', 'transform':'translate(-300px,0px)'})
        // sidebar.animate({
        //     "opacity": 0,
        //     }, function() {
        //         sidebar.css({'display':'none','transform':'translate(-300px,0px)'
        //     });
        // });
        padder.css({'margin-left':'0px'})
    }
}

function resize(){
    var sidebar = $('#sidebar');
    var padder = $('.content-padder');
    padder.removeAttr( 'style' );
    if( $( window ).width() < 960 && sidebar.css('display') == 'block' ) {
        sidebarToggle(false);
    } else if( $( window ).width() > 960 && sidebar.css('display') == 'none' ) {
        sidebarToggle(true);
    }
}
