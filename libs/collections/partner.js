import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);

export const Partners = new Mongo.Collection('Partners');

const Schemas = {};

Schemas.Partners = new SimpleSchema({
    name: {
        type: String,
        label: 'Name',
        max: 200,
    },
    account: {
        type: String,
        optional: true,
    },
    phone: {
        type: String,
        label: 'Phone',
    },
    email: {
        type: String,
        label: 'Email',
        optional: true
    },
    gender: {
        type: String,
        allowedValues: ['female', 'male', 'other'],
        autoform: {
            options: [
                { label: "Female", value: "female" },
                { label: "Male", value: "male" },
                { label: "Other", value: "other" }
            ]
        }
    },
    bio: {
        type: String,
        optional: true
    },
    address: {
        type: String,
        label: 'Adress',
        optional: true,
        autoform: {
            type: 'textarea',
        }
    },
    verify: {
        type: Boolean,
        defaultValue: false
    },
    //after user signup dont know that user has completed fill form or not
    complete: {
        type: Boolean,
        defaultValue: false
    },
}, { tracker: Tracker });

Partners.attachSchema(Schemas.Partners);

if (Meteor.isServer) {
    Partners.allow({
        insert: function() {
            return true;
        },
        update: function() {
            return true;
        },
        remove: function() {
            return true;
        }
    });

    Partners.after.insert(function(userId, doc) {

    });
    Partners.before.insert(function(userId, doc) {
        doc.account = userId;
    });
}