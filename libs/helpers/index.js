import { Partners} from '../collections';

Template.registerHelper('isUserLogin', function(id) {
    if (Meteor.userId() !== null) return true;
    return false;
});
// 11:01 AM | June 9
Template.registerHelper('formatDateChat', function(sdate) {
    if (sdate) return moment(sdate).format("hh:mm | MMM DD");
});

Template.registerHelper('FormatMMDD', function(sdate) {
    if (sdate) return moment(sdate).format("MMM DD");
});

Template.registerHelper('FormatDate', function(sdate) {
    if (sdate) return moment(sdate).format("DD MMMM YYYY");
});

Template.registerHelper('FormatYYYY', function(sdate) {
    if (sdate) return moment(sdate).format("YYYY");
});

Template.registerHelper('NamePartner', function(id) {
    if (id) {
        Meteor.subscribe('PublishPartnerById', id);
        let result = Partners.findOne({ _id: id });
        if (result !== undefined) return result.name;
    }
});

Template.registerHelper('verifypartner', function(partnerid) {
    if (partnerid !== '') {
        Meteor.subscribe('PublishPartnerById', partnerid);
        let result = Partners.findOne({ _id: partnerid });
        if (result !== undefined) {
            if (result.verify == true) return new Spacebars.SafeString('<i class="fa fa-check-circle verify-icon" aria-hidden="true"></i>');
        }
    }
});