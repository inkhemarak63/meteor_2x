Meteor.startup(function() {
    //Add defaul user roles
    var roles = ['admin', 'shop'];
    if (Meteor.roles.find().count() === 0) {
        roles.map(function(role) {
            Roles.createRole(role)
        })
    };
});
Meteor.methods({
    authResiter: function(fields) {
        console.log(fields);
        var userExists = Accounts.findUserByUsername(fields.username);
        if (!userExists) {
            if (fields.username !== '' && fields.password !== '') {
                var user = {username:fields.username, password: fields.password};
                var userId = Accounts.createUser(user);
                this.setUserId(userId);
                var checkrole = Roles.userIsInRole(userId, ['admin', 'shop']);
                if (checkrole == false) {
                    Roles.setUserRoles(this.userId, fields.role);
                    return {status: true, user_id: userId};
                }
                return {status: false, user_id: userId};
            }
        }
        return {status: false};
    },
    setRoleForExternal: function(role) {
        var checkrole = Roles.userIsInRole(this.userId, ['admin', 'shop']);
        if (checkrole === false) {
            if (role === "admin") {
                Roles.setUserRoles(this.userId, 'admin');
            } else if (role == 'shop') {
                Roles.setUserRoles(this.userId, 'shop');
            }
        }
    }
})
