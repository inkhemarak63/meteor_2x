import { Meteor } from 'meteor/meteor';
import { Partners, Province} from '/libs/collections';
import '/configs/user';
import '/libs/tabulars';
import './user';

Meteor.publish("roles", function() {
    return Meteor.roles.find({});
});
Meteor.publish('userData', function(userid) {
    return Meteor.users.find({ _id: userid });
});

Meteor.publish('userListAllByFilter', function(filter) {
    return Partners.find(filter);
});

Meteor.publish('signedinPartner', function() {
    return Partners.find({ account: this.userId });
});

Meteor.publish('PublishPartnerById', function(id) {
    return Partners.find({ _id: id });
});

Meteor.publish('PublishUserLogin', function(userid) {
    return Partners.find({ account: userid });
});

Meteor.startup(() => {
    // Province.insert({name: "Take", view_order: 1})
    Meteor.publish(null, function() {
        if (this.userId) {
            return Meteor.roleAssignment.find({ 'user._id': this.userId });
        } else {
            this.ready()
        }
    })
});

Meteor.methods({
    DeleteProvince: function(id) {
        if (!Roles.userIsInRole(this.userId, ['admin'])) return false;
        Province.remove({ _id: id });
        return true;
    },
})