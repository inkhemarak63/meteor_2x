import { Partners } from "../libs/collections/partner";
import Swal from 'sweetalert2';
import Util from './util';
import '/client/register';
Router.configure({
    layoutTemplate: 'MainLayout',
    notFoundTemplate: 'notFound',
    loadingTemplate: 'loading',
    waitOn() {
      Session.set('loadingSplash', false);
    },
    subscriptions() {
      return Meteor.subscribe('signedinPartner');
    },
    onRun() {
      if ( Meteor.isClient ) {
        if (Session.get('isAlertFillProfile')) {
            Swal.fire({
                title:  "Confirmation",
                text: "You must be field information",
                icon: 'info',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: "Ok"
              }).then((result) => {
                if (result.isConfirmed) {
                    Session.set('isAlertFillProfile', false);
                    Router.go("/profile");
                }
            })
        }
      }
      this.next();
    },
    onBeforeAction() {
        checkProfileInfo();
        this.next();
    }
});

Router.plugin('ensureSignedIn', {
    except: ['home', 'province', 'login', 'table','article', 'logout', 'register', 'atSignIn', 'atSignUp', 'atForgotPassword']
});


Router.route('/logout', function() {
    Meteor.logout(function(err) {
        if (!err) Router.go('/');
    });
});
//===================================\\
    // ***** Public Router ****** \\
//===================================//
Router.route('/', {
    name: 'home',
    subscriptions() {
      return Meteor.subscribe('signedinPartner');
    }
});

Router.route('/table', function() {
    this.render('table');
});
Router.route('/province', function() {
    this.render('province_record');
});
    //**** End Auth Router *****\\
//===================================


//===================================\\
    // ***** Auth Router ****** \\
//===================================//
Router.route('/login', function() {
    if(Meteor.userId() !== null)  Router.go('/');
    this.render('login');
});

Router.route('/register', function() {
    if(Meteor.userId() !== null)  Router.go('/');
    this.render('register');
});

Router.route('/profile', function() {
    this.render('profile');
});

    //**** End Auth Router *****\\
//===================================


//===================================\\
    // ***** Admin Router ****** \\
//===================================//
Router.route('/admin/member/list', function() {
    if(Roles.userIsInRole(Meteor.user(), ['admin'])) Router.go('/');
    this.render('admin_member_list');
}, { name: 'admin_member_list' });

   //**** End Admin Router *****\\
//===================================

function checkProfileInfo() {
    let name = Router.current().route.getName();
    let routes = ['public'];
    if (routes.indexOf(name) === -1) 
    {
        if(Roles.userIsInRole(Meteor.user(), ['admin', 'shop'])){
            let profile = Partners.findOne({account: Meteor.userId()});
            // check user blocked
            if (profile && profile.blocked) {
                Router.go('/terminated');
            }else if (!profile || !profile.isCompleted) {
                if (!profile && Session.get('isAlertFillProfile') === undefined) {
                    Session.set('isAlertFillProfile', true);
                }
                Router.go('/profile');
            }
        }
    }
}